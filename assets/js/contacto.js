            
$(function(){


    $("#frmEmail").submit(function(){

            var name = $("#nameCli").val();
            var email1 = $("#emailCli").val();
            var subjec = $("#subjectCli").val();
            var messaje = $("#messajeCli").val();
            var valido = true;

            if(name === null || name === ""){
                Swal.fire('Campo vacio!','debe de ingresar su nombre para poder enviar el correo','error');
                valido = false;
            }

            if(email1 === null || email1 === ""){
                Swal.fire('Campo vacio!','debe de ingresar su correo para poder contactarlo','error');
                valido = false;
            }

            if(subjec === null || subjec === ""){
                Swal.fire('Campo vacio!','debe de ingresar el motivo para poder enviar el correo','error');
                valido = false;
            }

            if(messaje === null || messaje === ""){
                Swal.fire('Campo vacio!','debe de ingresar el mensaje para poder enviar el correo','error');
                valido = false;
            }

            if(valido){
                
                $.ajax({
                    url:'https://dinarqro.com/app/index.php/Email/emaildo',
                    type:'POST',
                    dataType:'json',
                    data:{
                        namePet: name,
                        emailPet: email1,
                        subjecPet: subjet,
                        messajePet: messaje
                    },
                    success:function(json) {
                        if(json.response_code === "200"){
                            Swal.fire('Muy bien!', 'En cuanto veamos tu mensaje nos ponemos en contacto!', 'success');
                            console.log("si se envio ;)");
                        }
                    },
                    error : function(xhr, status) {
                        console.error("algo salio mal =(");
                    }
                });
            }


        return false;
    });
});
