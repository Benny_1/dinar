$(document).ready(function() {
    // mostrar el nombre del logo
    $(".nameLogo").show(1000);
    // animacion para ocultar el texto sobrante
    setTimeout(function(){ 
        $(".sobrante").fadeOut(2000);
    }, 2000);
    // animacion para ocultar el pre-acronimo
    setTimeout(function(){ 
        $("#preAcronimo").fadeOut(1000);
    }, 3200);
    // animacion para mostrar el acronimo
    setTimeout(function(){ 
        $("#acronimo").fadeIn(1000);
    }, 4000);
    // animacion para quitar el loader
    setTimeout(function(){ 
        $("#loader").fadeOut("slow");
    }, 5500);
});