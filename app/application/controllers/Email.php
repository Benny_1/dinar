<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Contorlador para el envio de correos 
* de la pagina dinar
* @author Noe Ramos Lopez
* @version 1.0
* @copyright derechos reservados 2020
*/
class Email extends CI_Controller {

	/**
	* @param namePet nombre del cliente
	* @param emailPet email del cliente 
	* @param subjecPet motivo del cliente
	* @param messajePet  messaje del cliente
	*/
	public function emaildo(){

		$jsonEmail = array();
		$nombre = $this->input->post('namePet');
		$email = $this->input->post('emailPet');
		$motivo = $this->input->post('subjecPet');
		$mensaje = $this->input->post('messajePet');
		$validForm = TRUE;

		// validamos que no esten vacios los campos
		if($nombre == '' || $nombre == NULL){
			$validForm == FALSE;
		}

		if($email == '' || $email == NULL){
			$validForm == FALSE;
		}

		if($motivo == '' || $motivo == NULL){
			$validForm == FALSE;
		}

		if($mensaje == '' || $mensaje == NULL){
			$validForm == FALSE;
		}

		// si no estan vacios procedemos a enviar el correo
		if($validForm){
			//se carga libreria de email
			$this->load->library('email');
			// de parte de quien es el correo
			$this->email->from($email, $nombre);
			// para quien es el correo
			$this->email->to('dinar.proyectos@outlook.com');
			$this->email->subject($motivo);
			$this->email->message("Hola el siguiente cliente tiene una dadu y manda el siguiente mensaje : ".$mensaje);

			$this->email->send();

			$jsonEmail['response_code'] = '200';
			$jsonEmail['response_msg'] = 'operacion exitosa';

		}else{
			$jsonEmail['response_code'] = '500';
			$jsonEmail['response_msg'] = 'algo salio mal =(';
		}
		
		echo json_encode($jsonEmail);
	}
}
